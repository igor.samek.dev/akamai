import pandas as pd
from io import StringIO
import logging

from .month_headers_generator import generate_month_headers

logger = logging.getLogger(__name__)


class CSVDataAnalyzer:
    def __init__(self, csv_content: str, passed_department: str, year: int | None, monthly_time_period: str | int | None):
        self.data = None
        self.load_csv_data(csv_content)
        self.available_departments = None
        self.target_department = passed_department
        self.year = year
        self.monthly_time_period = monthly_time_period
        self.month_headers = generate_month_headers(monthly_time_period)
        self.currency_sign = "$"

    def validate_department(self):
        if self.data is None:
            logger.info("CSV data not available")
        else:
            if self.target_department not in self.available_departments:
                logger.error(f"Target department not available. Available departments: {self.available_departments}")

    def prepare_data(self):
        if self.data is None:
            logger.info("CSV data not available")
        else:
            self.data = self.data[self.data['Department'] == self.target_department]
            if self.year is not None:
                self.data = self.data[self.data['Year'] == self.year]

            if self.month_headers is None:
                generate_month_headers(self.monthly_time_period)
            self.data = self.data[self.month_headers]
            self.convert_currency_to_numeric()

    def convert_currency_to_numeric(self):
        for month in self.month_headers:
            self.data[month] = self.data[month].replace({'\$': '', ',': '', '\(': '-', '\)': ''}, regex=True)
            self.data[month] = pd.to_numeric(self.data[month])

    def load_csv_data(self, csv_content: str) -> None:
        try:
            data_io = StringIO(csv_content)
            self.data = pd.read_csv(data_io, delimiter=';')
            self.available_departments = set(self.data["Department"])
            self.validate_department()
            logger.info("Stdin loaded successfully.")

        except (pd.errors.EmptyDataError, pd.errors.ParserError):
            logger.exception("Error while reading or parsing csv data.")
        except Exception as e:
            logger.exception(f"An unexpected error occurred: {e}")

    def sum_expenses(self):
        summed_expanses = self.data.sum()
        return f"{summed_expanses.iloc[0]}{self.currency_sign}"
