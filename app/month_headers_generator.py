import calendar


def is_month(potential_month: str) -> bool:
    potential_month = potential_month.capitalize()
    return True if potential_month in calendar.month_name[1:] else False


def generate_month_headers(month_param: str) -> list:
    month_headers = []

    if isinstance(month_param, str):
        month_headers.append(month_param[:3].capitalize())

    elif isinstance(month_param, int) and 1 <= month_param <= 4:
        for month_number in range((month_param - 1) * 3 + 1, month_param * 3 + 1):
            month_headers.append(calendar.month_name[month_number][:3].capitalize())

    else:
        for month_number in range(1, 13):
            month_headers.append(calendar.month_name[month_number][:3].capitalize())

    return month_headers
