import sys
import logging

from csv_data_analyzer import CSVDataAnalyzer

logger = logging.getLogger(__name__)


def prepare_args() -> dict:
    csv_analyzer_input = {}
    keys = ["target_department", "year", "month_param"]

    for i, key in enumerate(keys, start=1):
        try:
            csv_analyzer_input[key] = sys.argv[i]
        except IndexError:
            csv_analyzer_input[key] = None

    if csv_analyzer_input["year"] and csv_analyzer_input["year"].isdigit():
        csv_analyzer_input["year"] = int(csv_analyzer_input["year"])
    if csv_analyzer_input["month_param"] and csv_analyzer_input["month_param"].isdigit():
        csv_analyzer_input["month_param"] = int(csv_analyzer_input["month_param"])

    return csv_analyzer_input


def main():
    if 2 <= len(sys.argv) <= 4:
        csv_data_analyzer_input = prepare_args()
        analyzer = CSVDataAnalyzer(csv_data_analyzer_input["target_department"], csv_data_analyzer_input["year"],
                                   csv_data_analyzer_input["month_param"])
        analyzer.load_csv_data()
        analyzer.prepare_data()
        print(analyzer.sum_expenses())
    else:
        logger.error(f"Incorrect number of arguments. Expected 2-4, got {len(sys.argv) - 1}")


if __name__ == "__main__":
    main()
