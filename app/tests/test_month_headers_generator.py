import unittest

from app.month_headers_generator import is_month, generate_month_headers


class TestMonthHeadersGenerator(unittest.TestCase):

    def test_is_month_valid(self):
        self.assertTrue(is_month("January"))
        self.assertTrue(is_month("january"))
        self.assertFalse(is_month("Januarish"))

    def test_is_month_case_insensitive(self):
        self.assertTrue(is_month("january"))
        self.assertTrue(is_month("JANUARY"))
        self.assertTrue(is_month("January"))

    def test_generate_month_headers_string(self):
        self.assertEqual(generate_month_headers("January"), ["Jan"])
        self.assertEqual(generate_month_headers("january"), ["Jan"])

    def test_generate_month_headers_int_in_range(self):
        self.assertEqual(generate_month_headers(1), ["Jan", "Feb", "Mar"])
        self.assertEqual(generate_month_headers(2), ["Apr", "May", "Jun"])


if __name__ == '__main__':
    unittest.main()
