import calendar


from fastapi import FastAPI, File, UploadFile, Form
from typing import Union, Annotated

from starlette.responses import JSONResponse

from app.csv_data_analyzer import CSVDataAnalyzer

app = FastAPI()


@app.post("/expenses/")
async def calc_expenses(department: Annotated[str, Form()],
                        year: Annotated[int, Form()] = None,
                        monthly_time_period: Annotated[Union[int, str], Form()] = None,
                        csv_file: UploadFile = File(...)
                        ):
    try:
        if department is None:
            return JSONResponse(status_code=400, content={"message": "Parameter department is required."})

        if year is not None:
            str_year = str(year).replace('-', '')
            if len(str_year) != 4:
                return JSONResponse(status_code=400, content={"message": "Parameter year must be 4 digits long."})

        if monthly_time_period is not None:

            if isinstance(monthly_time_period, str) and monthly_time_period.isdigit():
                monthly_time_period = int(monthly_time_period)

            if isinstance(monthly_time_period, str):
                months_name = calendar.month_name[1:]
                if monthly_time_period not in months_name:
                    return JSONResponse(status_code=400, content={"message": "Your month must exists."})
            elif isinstance(monthly_time_period, int) and not (1 <= monthly_time_period <= 4):
                return JSONResponse(status_code=400, content={"message": "Choose quarter (1-4)."})

        csv_str_content = await csv_file.read()
        decoded_content = csv_str_content.decode("utf-8")
        try:
            analyzer = CSVDataAnalyzer(decoded_content, department, year, monthly_time_period)
            analyzer.prepare_data()
            return JSONResponse(status_code=200, content={"message": analyzer.sum_expenses()})

        except Exception:
            return JSONResponse(status_code=500, content={"message": "Something went wrong."})

    except ValueError as ve:
        return JSONResponse(status_code=422, content={"message": str(ve)})
    except Exception as ex:
        return JSONResponse(status_code=500, content={"message": str(ex)})
