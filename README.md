## Coding excercise

#### Branches:
- Task 1 - Task 1 only
- Main - Task 2 & 3 & 4

##### Link to task4:
- [Task4.md](./TASK4.md)

### Usage:
`docker-build-image.sh` - generate Docker image with this project  
`docker-run-container.sh` - run app with port 80 exposed

### Curls:
- Query the csv to get Sales expenses from January 2022
```bash
curl -X POST "http://localhost/expenses/" \
  -H "accept: application/json" \
  -H "Content-Type: multipart/form-data" \
  -F "department=Sales" \
  -F "year=2022" \
  -F "monthly_time_period=January" \
  -F "csv_file=@./app/Expenses.csv"
```

- Query the csv to get Payroll expenses from 2020
```bash
curl -X POST "http://localhost/expenses/" \
  -H "accept: application/json" \
  -H "Content-Type: multipart/form-data" \
  -F "department=Payroll" \
  -F "year=2020" \
  -F "csv_file=@./app/Expenses.csv"
```

- Query the csv to get all Sales expenses
```bash
curl -X POST "http://localhost/expenses/" \
  -H "accept: application/json" \
  -H "Content-Type: multipart/form-data" \
  -F "department=Sales" \
  -F "csv_file=@./app/Expenses.csv"
```


- Query the csv to get Marketing expenses from Q1 2020
```bash
curl -X POST "http://localhost/expenses/" \
  -H "accept: application/json" \
  -H "Content-Type: multipart/form-data" \
  -F "department=Marketing" \
  -F "year=2020" \
  -F "monthly_time_period=1" \
  -F "csv_file=@./app/Expenses.csv"
```
