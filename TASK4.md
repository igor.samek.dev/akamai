##### Example DB model for this project

![Task4 png](./task4.png)

##### Example DB query for this project

```sql
SELECT e.ExpenseID, d.DepartmentName, et.ExpenseTypeName, e.Year, e.Jan, e.Feb, e.Mar, e.Apr, e.May, e.Jun, e.Jul, e.Aug, e.Sep, e.Oct, e.Nov, e.Dec
FROM Expenses e
JOIN Departments d ON e.DepartmentID = d.DepartmentID
JOIN ExpenseTypes et ON e.ExpenseTypeID = et.ExpenseTypeID
WHERE e.ExpenseID = 1;
```